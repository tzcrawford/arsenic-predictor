**Mechanism of arsenic entering groundwater:**
- Anthropogenic sources
    - Dump sites, mines, smelters, coal/lumber burn, some pesticides
- Natural sources
    - 20th most abundant element in earth's crust -- average 2-3 mg/kg
    - Common in minerals like arsenopyrite (FeAsS), realgar (As4S4), orpiment (As2S3).
    - Weathering of rocks and sediments, geothermal activity, volcanoes, etc.
    
- Solubility
    - Reduction of As(V) (arsenate) --> As(III) (arsenite), more soluble and toxic
    - Usually exists as an oxide which binds to positive charges on soil particles (e.g. Fe)
    - Dissolved As concentration increases with pH
    - Sulfide can bind and precipitate As
    - Bacteria can expedite reduction of As or charges on soil particles. 
        - Both increases mobile As
        - Carbon can fuel the bacteria
        - Less carbon deep in groundwater
