#!/usr/bin/env python
import numpy as np
import folium
from folium import plugins
from vega_datasets import data
import pandas as pd
import geopandas as gpd

from interpolate_fns import gen_basemap_plot
from interpolate import bm

#Gets the request form options and calls the plot function with the appropriate parameters
def main(options):
    try:
        start_coords = (options['latitude'], options['longitude'])
        zoom_start=options['zoom_start']
    except:
        start_coords = (39.828175, -98.5795)
        zoom_start=4
    return plot(get_data_points(),start_coords,zoom_start)

#Creates the json for the plot
def plot(data_points,start_coords,zoom_start):

    folium_map = folium.Map(location=start_coords, zoom_start=zoom_start,
                            min_lat=25, max_lat=53, min_lon=-130, max_lon=-60,
                            tiles="Stamen Toner",
                            zoom_control=True
                           )

    folium_map.add_child(plugins.HeatMap(
                            data_points.dropna(axis=0).values
                        ,radius=10))


    chart = folium_map._repr_html_()
    return chart

#Gets the data poitns for the plot
def get_data_points():

    gdf_i_csv='data/gdf_i.csv'
    gdf=pd.read_csv(gdf_i_csv, na_values="NaN")
    gdf.rename(columns = {'lat':'LATITUDE', 'lon':'LONGITUDE'}, inplace = True)
    #gdf.set_index('???', inplace=True)
    data_points=gdf[['LATITUDE','LONGITUDE','Ground As']]

    #as_shape_file='data/arsenic_nov2001_shp/arsenic_nov2001.shp'
    #gdf=gpd.read_file(as_shape_file)
    #gdf.rename(columns = {'LAT_DD':'LATITUDE', 'LON_DD':'LONGITUDE', 'WELLDPTH_': 'WELLDEPTH'}, inplace = True)
    #gdf.set_index('STAID', inplace=True)
    #data_points=gdf[['LATITUDE','LONGITUDE','AS_CONC']]

    print(data_points)
    return data_points

def gen_basemap_plot_from_gdf(gdf,z_col):
    
    lon = gdf['LONGITUDE'].to_numpy()
    lat = gdf['LATITUDE'].to_numpy()
    numrows = max(np.unique(lon,return_counts=True)[1])
    numcols = max(np.unique(lat,return_counts=True)[1])
    m_lon, m_lat = bm(*(lon, lat))
    xi=m_lon.reshape(numrows,numcols)
    yi=m_lat.reshape(numrows,numcols)
    zi = gdf[z_col].to_numpy().reshape(numrows,numcols)

    return gen_basemap_plot(bm, xi, yi, zi)

if __name__ == '__main__':
    #data_points = get_data_points()
    gdf_i_csv='data/gdf_i.csv'
    gdf=pd.read_csv(gdf_i_csv, na_values="NaN")
    gdf.rename(columns = {'lat':'LATITUDE', 'lon':'LONGITUDE'}, inplace = True)
    
    gen_basemap_plot_from_gdf(gdf,'Ground Mo')

