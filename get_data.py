#!/usr/bin/env python

import os
import requests
import zipfile

link_as_in_water = "https://water.usgs.gov/GIS/dsdl/arsenic_nov2001_shp.zip"
link_geochem     = "https://mrdata.usgs.gov/geochem/ngs.zip"

if not os.path.isdir("data"):
    os.mkdir("data")

if not os.path.isdir("data/arsenic_nov2001_shp"):
    if not os.path.isfile("data/arsenic_nov2001_shp.zip"):
        response = requests.get(link_as_in_water)
        file = open("data/arsenic_nov2001_shp.zip", 'wb')
        file.write(response.content)
        file.close()
    with zipfile.ZipFile("data/arsenic_nov2001_shp.zip", 'r') as zip_ref:
        zip_ref.extractall("data/arsenic_nov2001_shp")

if not os.path.isdir("data/ngs"):
    if not os.path.isfile("data/ngs.zip"):
        response = requests.get(link_geochem)
        file = open("data/ngs.zip", 'wb')
        file.write(response.content)
        file.close()
    with zipfile.ZipFile("data/ngs.zip", 'r') as zip_ref:
        zip_ref.extractall("data/ngs")

