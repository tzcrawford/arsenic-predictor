#!/usr/bin/env python
import numpy as np
import pandas as pd
import geopandas as gpd
from os.path import exists
#from sklearn.compose import ColumnTransformer
from sklearn.model_selection import GridSearchCV
from sklearn.utils import shuffle
from sklearn.pipeline import Pipeline
from sklearn.linear_model import Ridge
from sklearn.impute import SimpleImputer
import joblib

from model_and_predict import input_columns, result_column


def make_model(alpha=1.0):
    model = Pipeline([
                        ('imputer', SimpleImputer(strategy='constant',fill_value=None)),
                        ('ridge', Ridge(alpha=alpha))
                   ])
                        #('imputer', SimpleImputer()),
    return model
model=make_model()

def prep_data(df, input_columns=input_columns, result_column=result_column):
    X=df[input_columns]
    y=df[result_column]
    X_shuf,y_shuf = shuffle(X,y,random_state=10234)
    return X_shuf, y_shuf

def fit_data(model, df, input_columns=input_columns, result_column=result_column):

    X_shuf,y_shuf = prep_data(df, input_columns=input_columns, result_column=result_column)
    model.fit(X_shuf, y_shuf)

    print("Model Score: ", model.score(X_shuf,y_shuf))

    return model

def cross_validate(df):
    gs = GridSearchCV(
        model,
            {
                "ridge__alpha": np.array(range(2,13))/10
            },
        cv=10,
        n_jobs=8
    )
    X_shuf,y_shuf = prep_data(df, input_columns=input_columns, result_column=result_column)
    gs.fit(X_shuf,y_shuf)
    return gs.best_params_

def score_model(model, df, input_columns=input_columns, result_column=result_column):
    X_shuf,y_shuf = prep_data(df, input_columns=input_columns, result_column=result_column)
    score = model.score(X_shuf,y_shuf)
    return score

def save_model(model, location='data/model.joblib'):
    return joblib.dump(model, location)
def load_model(location='data/model.joblib'):
    return joblib.load(location)


if __name__=='__main__':
    df = pd.read_csv('data/gdf.csv', na_values="NaN")

    model_loc='data/model.joblib'
    if exists(model_loc):
        model = load_model(model_loc)
    else:
        model = fit_data(make_model(alpha=1.2), df, input_columns, result_column)
        print("Best parameters from cross validation: ", cross_validate(df))
        #save_model(model, model_loc)
