#!/usr/bin/env python
import numpy as np
from copy import copy, deepcopy
import geopandas as gpd
from mpl_toolkits.basemap import Basemap
from scipy.spatial import KDTree
import pickle

from interpolate import load_interpolated_z_col as load_interpolated
from interpolate import bm #basemap for converting to/from map projection
from interpolate_fns import llon,llat,ulon,ulat

def get_well_water_data(llon=llon, llat=llat, ulon=ulon, ulat=ulat, \
                        well_water_shapefile='data/arsenic_nov2001_shp/arsenic_nov2001.shp', \
                        verbose=False, point_lim=False):
    # Collect survey data on arsenic in well water in gpd form

    #The geometry column is not in the same units as decimal degrees?
    gdf_r=gpd.read_file(well_water_shapefile)[['LON_DD','LAT_DD','AS_CONC','WELLDPTH_']]
    gdf_r.rename(columns={'LON_DD': 'lon', 'LAT_DD': 'lat', 'WELLDPTH_': 'WELLDEPTH'}, inplace=True)
    if(verbose):
        print("Number of rows before dropping: {}".format(len(gdf_r)))
    gdf_r.dropna(subset=['AS_CONC'], inplace=True) #drop rows where not defined
    gdf_r=gdf_r[ (gdf_r.lon > llon) & (gdf_r.lon < ulon) & (gdf_r.lat > llat) & (gdf_r.lat < ulat)] #drop rows with cordinates outside set range (AK & HI)
    if(point_lim):
        gdf_r=gdf_r[:point_lim] #limit to small number of points just to speed things up
    if(verbose):
        print("Number of rows after dropping: {}".format(len(gdf_r)))
    
    gdf_r=gdf_r.reset_index(drop=True) #if lines were dropped, we want to reset count for index column so we're not missing values
    return gdf_r

def get_grid_data(interpolated_save):
    # Gets the grid data produced in interpolate.py

    xi, yi, zi, numcols, numrows, z_col, interp_method = \
        load_interpolated(interpolated_save)
    assert xi.shape == yi.shape
    assert xi.shape[0] == numrows
    assert xi.shape[1] == numcols
    
    print("Grid shape: {}".format(xi.shape))
    print("Interpolation method was: {}".format(interp_method))
    
    # Convert from map coordinates back to regular coordinates
    lon, lat = bm(xi, yi, inverse=True)
    #These are the arrays as a meshgrid
    lon_grid=copy(lon) ; lat_grid=copy(lat)
    #These are the series of longitudes and latitudes
    lon=lon[0,:] ; lat=lat[:,0]
    #print(lon)
    #print(lat)

    return xi, yi, zi, lon_grid, lat_grid, lon, lat

def get_grid_df(interpolated_saves, verbose=False):
    # Takes the grid data from interpolate.py and turns that into an input vector in gdf form
    # interpolated_saves is a dictionary 
        #key: each column of training data
        # value: the file of interpolated data for that column

    if verbose:
        print("Generating dataframe of interpolated data for: {}".format(interpolated_saves))

    lon_grids=list(); lat_grids=list()
    gdf_i=dict()
    for k,v in interpolated_saves.items():
        zi, lon_grid, lat_grid = get_grid_data(v)[2:5]
        lon_grids.append(lon_grid)
        lat_grids.append(lat_grid)
        gdf_i[k]=zi.flatten()

    # We make sure all of the interpolated parameters are on the same grid
    for lon_grid in lon_grids:
        assert np.array_equal(lon_grid, lon_grids[0])#, equal_nan=True)
    for lat_grid in lat_grids:
        assert np.array_equal(lat_grid, lat_grids[0])#, equal_nan=True)
    gdf_i['lon']=lon_grids[0].flatten()
    gdf_i['lat']=lat_grids[0].flatten()

    gdf_i = gpd.GeoDataFrame(gdf_i) #Convert to geopandas dataframe
    if(verbose):
        print("Number of rows before dropping: {}".format(len(gdf_r)))
    #for k in interpolated_saves.keys():
    #    gdf_i.dropna(subset=[k], inplace=True) #drop rows where not defined
    if(verbose):
        print("Number of rows after dropping: {}".format(len(gdf_r)))

    gdf_i=gdf_i.reset_index(drop=True) #if lines were dropped, we want to reset count for index column so we're not missing values
    return gdf_i

def match_to_interpolated(gdf_r, gdf_i):
    # Copies the gdf of the result data, but replaces the lat and lon columns with the lat and lon from the nearest point in the interpolated grid

    #Create numpy arrays out of the coordinates so we can run KDTree on it
    n_i = np.array(list(gdf_i.apply( lambda x: (x.lon, x.lat), axis=1 )))
    n_r = np.array(list(gdf_r.apply( lambda x: (x.lon, x.lat), axis=1 )))

    tree = KDTree(n_i) # This makes a data structure where we can quickly look through n_i
    dist, idx = tree.query(n_r, k=1)
    
    #This makes a copy of gdf_i, but with each row corresponding to the matching row in gdf_r
    gdf_nearest_i = gdf_i.iloc[idx].reset_index(drop=True)
    #needed to reset index so that we can join on this index later (missing values)
    gdf_nearest_i.dropna(subset=['lon'], inplace=True) 

    #Join the gdf_i columns onto gdf_r, such that each row has matching result data location 
        # and the location of the nearest point in gdf_i
    gdf = gdf_r.join(gdf_nearest_i, rsuffix='_i') 

    return gdf

if __name__ == '__main__':
    #Get the well water data (result vector)
    gdf_r = get_well_water_data(verbose=True)
    #print(gdf_r)
    
    #Get the grid data (input parameters)
    interpolated_saves = {
                            'Ground As': 'data/temp.pkl'
                         }
    gdf_i = get_grid_df(interpolated_saves,verbose=True)
    #print(gdf_i)
    
    #Match the result vector to input data at the nearest point on the interpolated grid
    gdf = match_to_interpolated(gdf_r, gdf_i)
    print(gdf)
