# arsenic-predictor
## SUMMARY
My capstone project for The Data Incubator.
It predicts arsenic in groundwater in the United States using information such as element concentrations in surrounding earth material.
The project is free and open source and uses publicly available data sets.
Final version intends to include user-interactive maps, allows user input to give numerical prediction, perhaps gives summary of predominant risk factors for that area.

Dissolved arsenic is a toxic component of drinking water which can be [sourced](./notes/mechanism_of_As_contamination.md) from natural minerals or anthropogenic mining or agricultural waste [4].
Fourteen percent of the United States population (46 M people) relies on wells for household water [1].
Importantly, regulation is loose and people are responsible for managing their own water sources and ensuring they are safe to drink from.
At the same time, literature predicts that something like seven percent of existing wells are actually above the EPA threshold of 10 ug/L [2].
While there is still ongoing discussion about what concentrations constitute safe levels of arsenic concentration ([As]), most public health concerns come from chronic exposure from groundwater.
Geolocational proximity to contaminated sources is often a poor predictor for contamination of any particular well.
So publicly available resources are useful for informing individuals who either currently have or are thinking about building well water sources on their property.


## INPUT PARAMETERS (PREDICTORS)
- Concentration of Arsenic (As), Antimony (Sb), and Berylium (Be) in surrounding earth material
    - Literature [1] predicts this should positively correlate with [As] in well water
- Concentration of Bismuth (Bi) and Molybdenum (Mo) in surrounding earth material
    - Literature [1] predicts this should negatively correlate with [As] in well water
- Well depth
    - Probably related, but not explored much in literature
- Some other parameters I want to include:
    - Precipitation
        - More arid areas are considered more likely to have contaminated water sources
    - Climate/Temperature
    - Elevation 
    - pH
        - Highly correlated but very difficult to measure on a grand scale


## DATA SOURCES
- [USGS National Geochemical Survey](https://mrdata.usgs.gov/geochem/doc/home.htm)
    - Provides concentrations of chemical elements historically measured in stream sediment
    - 77,000+ samples, 61 MiB
    - 89% coverage every 17x17 km
    - Sampled from 1970s-2008. Age generally not considered a problematic/leading to inconsistency.
    - Compiled from a variety of existing studies/analyses in USGS databases.
        - Multiple datasets and methods/techniques
        - Sometimes uneven in quality and elemental coverage
- [USGS Map of Arsenic concentrations in groundwater of the United States](https://pubs.usgs.gov/wri/wri994279/)
    - Some actual wells and other groundwater sources measured for [As]
    - 20,000+ water sources, 1.6 MiB
    - Somewhat clustered spatially
    - Sampled 1973-1998
    - Represent groundwater sources in general, not just drinking water. But excludes water sources >2 g/L dissolved solids or >50°C


## CITATIONS
- [1] Ayotte, J.D. et al. "Estimating the High-Arsenic Domestic-Well Population in the Conterminous United States." In *Environmental Science & Technology* (**2017**),  [doi:10.1021/acs.est.7b02881](http://doi.org/10.1021/acs.est.7b02881)
- [2] Lombard, M.A. et al. "Assessing the Impact of Drought on Arsenic Exposure from Private Domestic Wells in the Conterminous United States." In *Environmental Science & Technology* (**2021**), [doi:10.1021/acs.est.9b05835](http://doi.org/10.1021/acs.est.9b05835)
- [3] Lombard, M.A. et al. "Machine Learning Models of Arsenic in Private Wells Throughout the Conterminous United States As a Tool for Exposure Assessment in Human Health Studies." In *Environmental Science & Technology* (**2021**) [doi:10.1021/acs.est.0c05239](http://doi.org/10.1021/acs.est.0c05239)
- [4] University of Maine "How does arsenic get into groundwater?" [https://umaine.edu/arsenic/how-does-arsenic-get-into-the-groundwater/](https://umaine.edu/arsenic/how-does-arsenic-get-into-the-groundwater/)

## SCRIPTS
- `flaskapp.py`: Main script that web server will run to allow user to interact with visualize data
- `plot.py`: Script that will define how data is plotted either on the flaskapp or externally
- `get_data.py`: Script that will acquire data project is built around.
- `model_and_predict.py`: Wrapper script to perform all of the steps preparing the data, model, and predictions
- `interpolate.py`: Script that takes input data and forms an evenly-spaced grid of expected (interpolated) values. This is necessary to match points of different data sets together.
    - Uses Inverse-Distance-Weighted (IDW) or cubic interpolation.
- `interpolate_fns`: Just helper functions for `interpolate.py`
- `ml_prep.py`: Collects input information into usable data structures for passing into machine learning or plotting scripts
- `machine_learning.py`: Performs the actual machine learning model construction


## TO DO
### For minimum viable product
- Implement plotting/display of predicted results
- Implement numerical prediction based upon user input on the flaskapp
- Improve machine learning algorithm.
    - Current simple ridge regression only gives of pitiful R^2 of 0.03% among training data with cubic interpolation. (*EEK*)
    - Consider how imputation of missing areas affects the interpolated data that is fed into the ML algorithm. Some of the input variables only partially cover regions of the USA.
    - Double check/debug that I am extracting geometry/coordinates of points correctly. I think I am, but this would be a big contributor to predictions being poor
- Figure out how exactly I will be making plots on the flaskapp. 
    - Folium probably isn't the way to go actually
    - Basemap is working well but need to figure out how to embed that in html
### Nice-to-haves
- Mask the data so that it all fits within the US Borders not going into the ocean or Canada/Mexico
- Implement other input parameters such as elevation, precipitation, climate, water table structure, etc.
- Give summary information about particular location or user input. Identify risk factors, etc.
- Could consider a statistical spatial interpolation instead of deterministic
- Optimize IDW interpolation. It currently makes massive arrays in swap files on the scale of gigabytes.

## COMPLETED
- Motivation/writeup in README
- Composed flask app
    - **Template** html and user input forms
    - Takes data and produces plots based on user input (currently using folium which needs to change)
    - Deployed on remote server (unpublished)
- Script to collect publicly available data
- Implemented mechanism to compare datasets which include datapoints at different locations/coordinates
    - Uses deterministic interpolation raster/grid
        - Inverse distance weighted method (probably most common way of doing this)
        - Faster `scipy.interpolate.griddata` "cubic" method
    - Arranges predictor data on raster grid
    - Matches measured result data to nearest grid point
    - Loads this into a pandas dataframe where each column is an input parameter/the result and each row is one of the observed results matched with the nearest interpolated values for each input parameter
- Script performing machine learning modeling to predict groundwater arsenic concentration using the aforementioned pd dataframe.
- Script to plot input data via basemap (external to the flaskapp)
