#!/usr/bin/env python
import numpy
import pandas as pd
from os.path import exists
from os import remove

import interpolate
import ml_prep
import machine_learning


#Data information
#input_columns = ['WELLDEPTH', 'Ground As', 'Ground Be', 'Ground Sb', \
#                 'Ground Bi', 'Ground Mo']
input_columns = ['WELLDEPTH', 'Ground As', 'Ground Be', 'Ground Mo']
result_column = 'AS_CONC'

# Interpolation parameters
    # Method of interpolation, 'cubic' or 'idw' (inverse distance weighted)
interp_method = 'cubic' 
    # Granularity of interpolation grid
#numcols=240
#numrows=241
numcols=10000
numrows=10000

#Where the machine learning model will be saved
model_loc='data/model.joblib'

# Declare input parameters to machine learning model
interpolated_cols = {
                        "Ground As": "AS_ICP40",
                        "Ground Be": "BE_ICP40",
                        "Ground Mo": "MO_ICP40"
                    }
                        #"Ground Sb": "SB_ICP40",
                        #"Ground Bi": "BI_ICP40",
interpolated_saves = {
                        "Ground As": "data/ground_As.pkl",
                        "Ground Be": "data/ground_Be.pkl",
                        "Ground Mo": "data/ground_Mo.pkl"
                     }
                        #"Ground Sb": "data/ground_Sb.pkl",
                        #"Ground Bi": "data/ground_Bi.pkl",
assert interpolated_cols.keys() == interpolated_saves.keys()

# ML Prep dataframe save locations
gdf_prep_saves = {
                    "gdf": "data/gdf.csv",
                    "gdf_i": "data/gdf_i.csv",
                    "gdf_r": "data/gdf_r.csv",
                 }

def generate_iterpolated_data(interp_method=interp_method, numcols=numcols, \
                              numrows=numrows, interpolated_cols=interpolated_cols, \
                              interpolated_saves=interpolated_saves):
    # Generate interpolated data files for each input parameter
    for param in interpolated_cols.keys():
        #If the interp. data has already been generated
        if( exists(interpolated_saves[param]) ): 
            print("Interpolation for {} already exists at {}"\
                  .format(param, interpolated_saves[param]))
            continue
        
        print("Generating interpolated data for {} at {}"\
              .format(param, interpolated_saves[param]))
        interpolate.interpolate_z_col(z_col=interpolated_cols[param], \
                                      save=interpolated_saves[param], \
                                      numcols=numcols, numrows=numrows, \
                                      interp_method=interp_method,
                                      gen_plot=False)
    
    return

def prep_ml_data_frames(interpolated_saves=interpolated_saves, gdf_prep_saves=gdf_prep_saves):
    if not exists(gdf_prep_saves['gdf']):
        # Create the ml input and result vectors
        gdf_r = ml_prep.get_well_water_data(verbose=False)
        gdf_i = ml_prep.get_grid_df(interpolated_saves, verbose=False)
        gdf = ml_prep.match_to_interpolated(gdf_r,gdf_i)
        
        print("gdf_r:\n", gdf_r, "\n")
        print("gdf_i:\n", gdf_i, "\n")
        print("gdf:\n",   gdf,   "\n")

        gdf_r.to_csv(gdf_prep_saves['gdf_r'],na_rep='NaN', index=False)
        gdf_i.to_csv(gdf_prep_saves['gdf_i'],na_rep='NaN', index=False)
        gdf.to_csv(  gdf_prep_saves['gdf'],  na_rep='NaN', index=False)
    else:
        # Load existing ml input and result vectors
        print("Dataframes for machine learning already prepared at: ", *gdf_prep_saves.values())
        gdf   = pd.read_csv(gdf_prep_saves['gdf'],   na_values="NaN")
        gdf_i = pd.read_csv(gdf_prep_saves['gdf_i'], na_values="NaN")
        gdf_r = pd.read_csv(gdf_prep_saves['gdf_r'], na_values="NaN")

    return gdf, gdf_i, gdf_r

def perform_machine_learning(gdf, alpha=1.0,model_loc=model_loc):
    # Create machine learning model or load it if it exists
    if exists(model_loc):
        print("Machine learning model already exists at: ", model_loc)
        model = machine_learning.load_model(model_loc)
    else:
        model = machine_learning.fit_data(
                machine_learning.make_model(alpha=alpha),
                gdf,
                machine_learning.input_columns,
                machine_learning.result_column
            )
        print("Best parameters from cross validation: ", \
              machine_learning.cross_validate(gdf))
        machine_learning.save_model(model, model_loc)
    return model

def delete_files(model_loc=model_loc, interpolated_saves=interpolated_saves,gdf_prep_saves=gdf_prep_saves):
    print("Deleting files associated with ml fit")
    for i in [model_loc, *interpolated_saves.values(), *gdf_prep_saves.values() ]:
        try:
            remove(i)
        except Exception as e:
            print(e)
    return

if __name__ == '__main__':
    #delete_files()
    generate_iterpolated_data()
    gdf, gdf_i, gdf_r = prep_ml_data_frames()
    model = perform_machine_learning(gdf, alpha=1.0)
    print("Model score: ", machine_learning.score_model(model, gdf) )
