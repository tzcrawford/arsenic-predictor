#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import geopandas as gpd
from scipy.interpolate import griddata
from mpl_toolkits.basemap import Basemap

### Define range of coordinates for map of USA ###
llon = -125
llat = 25
ulon = -65
ulat = 50

def distance_matrix(x0, y0, x1, y1):
    #Helper function for simple_idw

    #print("Shape of x0: {}".format(x0.shape)) #(rows used from ngs data,)
    #print("Shape of x1: {}".format(x1.shape)) #(numrows*numcols,)

    obs =    np.vstack((x0, y0)).T #Shape (len(x0, 2))
    interp = np.vstack((x1, y1)).T #Shape (len(x1, 2))

    # Make a distance matrix between pairwise observations
    # Note: from <http://stackoverflow.com/questions/1871536>
    #d0 = np.subtract.outer(obs[:,0], interp[:,0]) #Shape (len(x0),2)
    #d1 = np.subtract.outer(obs[:,1], interp[:,1]) #Shape (len(x0),numrows*numcols)

    f1 = np.memmap('data/idw_swap_1.npy', dtype='float16', mode='w+', shape=(len(x0),len(x1)) )
    f2 = np.memmap('data/idw_swap_2.npy', dtype='float16', mode='w+', shape=(len(x0),len(x1)) )
    f1[:] = np.subtract.outer(obs[:,0], interp[:,0]) #Shape (len(x0),2)
    f2[:] = np.subtract.outer(obs[:,1], interp[:,1]) #Shape (len(x0),numrows*numcols)

    #hypot = np.hypot(d0, d1) 
    #return hypot
    
    f1[:]= np.hypot(f1, f2) 
    #f1[:]= np.hypot(
    #                np.subtract.outer(obs[:,0], interp[:,0]),
    #                np.subtract.outer(obs[:,1], interp[:,1])
    #               )
    return f1 


def simple_idw(x, y, z, xi, yi):
    #Simple inverse-distance-weighted interpolation algorithm

    #dist = distance_matrix(x,y, xi,yi)
    f1 = distance_matrix(x,y, xi,yi)

    # In IDW, weights are 1 / distance
    #weights = 1.0 / dist
    f1[:] = 1.0 / f1

    # Make weights sum to one
    #weights /= weights.sum(axis=0)
    f1[:] /= f1.sum(axis=0)

    # Multiply the weights for each interpolated point by all observed Z-values
    #zi = np.dot(weights.T, z)
    zi = np.dot(f1.T, z)
    print(zi.shape)

    return zi


def generate_fake_data(n,ulat=ulat,llat=llat,ulon=ulon,llon=llon,extra_deg=2,zdat_shift=8):
    # Generate some fake data that we can interpolate on
    # Just following what was done at https://earthscience.stackexchange.com/questions/12057/how-to-interpolate-scattered-data-to-a-regular-grid-in-python/12059#12059?newreg=42b2a6748d464692ad2a29404f6ca138

    # Note that these create a normal distribution, but all data will be in the range of the limits low, high
    lat = np.random.uniform(low=llat+extra_deg, high=ulat-extra_deg, size=n)
    lon = np.random.uniform(low=llon+extra_deg, high=ulon-extra_deg, size=n)

    # I don't think this is really necessary, but whatever
    lat = np.append(lat, [llat, ulat, ulat, llat])
    lon = np.append(lon, [llon, ulon, llon, ulon])
    #We have to add four to the array size because we added the bounds to the array twice
    zdat = np.random.randn(n+4) + zdat_shift

    return lat, lon, zdat


def gen_lat_lon_columns(gdf, lat_column='lat', lon_column='lon', geometry_column='geometry'):
    # From the geometry column of a geopandas dataframe, make separate columns containing the degrees of longitude and latitude

    gdf[lon_column]=gdf[geometry_column].map(
        lambda x: float(str(x).strip("POINT (").replace(" ",",").strip(")").split(",")[0])
        )
    gdf[lat_column]=gdf[geometry_column].map(
        lambda x: float(str(x).strip("POINT (").replace(" ",",").strip(")").split(",")[1])
        )
    return gdf


def get_ngs_data(llon=llon, llat=llat, ulon=ulon, ulat=ulat, \
                z_col='AS_ICP40', geochem_shapefile='data/ngs/ngs.shp', \
                verbose=False, point_lim=False):
    # Collect national geochemical survey data

    gdf=gpd.read_file(geochem_shapefile)[[z_col,'geometry']]
    gdf=gen_lat_lon_columns(gdf)
    if(verbose):
        print("Number of rows before dropping: {}".format(len(gdf)))
    gdf.dropna(subset=[z_col], inplace=True) #drop rows where z_col is not defined
    gdf=gdf[ (gdf.lon > llon) & (gdf.lon < ulon) & (gdf.lat > llat) & (gdf.lat < ulat)] #drop rows with cordinates outside set range (AK & HI)
    if(point_lim):
        gdf=gdf[:point_lim] #limit to small number of points just to speed things up
    if(verbose):
        print("Number of rows after dropping: {}".format(len(gdf)))

    return gdf


def interpolate(m_lon,m_lat,zdat,numrows,numcols,interp_method='cubic',verbose=False,gridrange=False):
    #Generate an evenly spaced grid of interpolated data
        #Gridrange is an optional parameter where you pass a dictionary containing map
    
    #Generate an evenly-spaced grid of latitude & longitude points on the range of input data
        # (already transformed to map proj)
    if gridrange:
        xi = np.linspace(gridrange['lon_min'], gridrange['lon_max'], numcols)
        yi = np.linspace(gridrange['lat_min'], gridrange['lat_max'], numrows)
    else: #automatically determine the range of coordinates to form the grid over
        xi = np.linspace(m_lon.min(), m_lon.max(), numcols)
        yi = np.linspace(m_lat.min(), m_lat.max(), numrows)
    if(verbose):
        print("Shape of xi:    {}".format(xi.shape))
        print("Shape of yi:    {}".format(yi.shape))
    
    xi, yi = np.meshgrid(xi, yi) #need to pair x & y points


    ### Interpolate what z should be ###
    #Cubic method
    if(interp_method == 'cubic'):
        zi = griddata((m_lon,m_lat),zdat,(xi,yi),method='cubic')
    #Inverse-distance-weighted method
    if(interp_method == 'idw'):
        xi, yi = xi.flatten(), yi.flatten()
        zi = simple_idw(m_lon, m_lat, zdat, xi, yi).reshape(numrows,numcols)
        #Needs to be (numrows,numcols) not (numcols,numrows) ; will give error if incorrect
        xi = xi.reshape(numrows,numcols)
        yi = yi.reshape(numrows,numcols)
        zi = zi.reshape(numrows,numcols)
    
    if(verbose):
        print("zi[:10] = {}".format(zi[:10]))
        print("Final shape of m_lon: {}".format(m_lon.shape))
        print("Final shape of m_lat: {}".format(m_lat.shape))
        print("Final shape of xi:    {}".format(xi.shape))
        print("Final shape of yi:    {}".format(yi.shape))
        print("Final shape of zdat:  {}".format(zdat.shape))
        print("Final shape of zi:    {}".format(zi.shape))

    return xi, yi, zi


def gen_basemap_plot(bm, xi, yi, zi):
   
    fig, ax = plt.subplots(figsize=(12, 12)) #Produce figure
    cs = bm.contourf(xi, yi, zi, 500, cmap='magma', zorder = 2) # Plot interpolated points
    
    #Draw other map features
    bm.drawmapboundary(fill_color = 'skyblue', zorder = 1)
    bm.drawlsmask(ocean_color='skyblue', land_color=(0, 0, 0, 0), lakes=True, zorder = 4)
    bm.drawcoastlines()
    bm.drawcountries(color='grey',linewidth=1)
    bm.drawstates(color='lightgrey', linewidth=1)
    
    cbar = plt.colorbar()
    #plt.title('Arsenic concentration in sediment')
    
    plt.show()

def get_grid_range(bm, llon=llon, llat=llat, ulon=ulon, ulat=ulat):
    # Creates gridrange dictionary for interpolate function 
        #using declared upper and lower longitude/latitude

    lons=np.array([llon,ulon])
    lats=np.array([llat,ulat])
    m_lons, m_lats = bm(*(lons,lats)) #transform to map projection
    
    gridrange = dict()
    gridrange['lon_min']=m_lons.min()
    gridrange['lon_max']=m_lons.max()
    gridrange['lat_min']=m_lats.min()
    gridrange['lat_max']=m_lats.max()

    return gridrange
