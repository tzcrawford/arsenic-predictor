#!/usr/bin/env python
import numpy as np
import geopandas as gpd
from mpl_toolkits.basemap import Basemap
import pickle

from interpolate_fns import generate_fake_data, \
                            get_ngs_data, \
                            interpolate, \
                            gen_basemap_plot, \
                            llon, llat, ulon, ulat, \
                            get_grid_range
    
bm = Basemap(projection = 'merc', resolution='i', \
            llcrnrlon = llon, llcrnrlat = llat, urcrnrlon = ulon, urcrnrlat = ulat) 


def interpolate_z_col(z_col='AS_ICP40', fake_points=False, \
                      interp_method='cubic', numcols=240, numrows=241, \
                      save=False, gen_plot=False, verbose=False):
    # Make a grid interpolation
        # z_col defines which element/experiment from the National Geochemical Survey to use
        # fake_points (optional) will instead produce fake data with the number of points given
        # interp_method will determine whether to interpolate the data with 
            # 'cubic' or 'idw' (inverse distance weighted) algorithm
        # numcols and numrows are the dimensions of discrete lon/lat steps for output arrays
            # note output xi, yi do not match these dimensions because basemap requires the data to go through meshgrid
        # save will define a path to save the xi,yi,zi arrays for recovery later
        # gen_plot will optionally open a basemap plot to show the generated data

    ### Collect Source Data ###
    
    if fake_points:
        ## Fake Data ##
        lat, lon, zdat = generate_fake_data(fake_points, ulat, llat, ulon, llon)
    else:
        ## Data from National Geochemical Survey ##
        gdf = get_ngs_data(llon, llat, ulon, ulat, z_col=z_col, verbose=verbose)##, point_lim=100)
        
        # Alias these columns to be consistent with randomly generated data names. 
            # Note that changing lat changes gdf['lat'] and vice-versa
        lat = gdf['lat'] ; lon = gdf['lon'] ; zdat = gdf[z_col] 
    
    # transform coordinates to map projection
    m_lon, m_lat = bm(*(lon, lat))
    #m_lon, m_lat = lon, lat
    #print(m_lon)
    
    # Generate grid data
    xi, yi, zi = interpolate(m_lon, m_lat, zdat, numrows, numcols, \
                             interp_method=interp_method, verbose=verbose, \
                             gridrange=get_grid_range(bm)
                            )
    
    
    if gen_plot:
        gen_basemap_plot(bm, xi, yi, zi)

    if save:
        with open(save,'wb') as f:
            pickle.dump([xi,yi,zi,numcols,numrows,z_col, interp_method], f)

    return xi, yi, zi

def load_interpolated_z_col(save, gen_plot=False):
    # Load the grid interpolation already created before
        # save is the save file path

    with open(save,'rb') as f:
        xi, yi, zi, numcols, numrows, z_col, interp_method = pickle.load(f)

    if gen_plot:
        bm = Basemap(projection = 'merc', resolution='i', \
                 llcrnrlon = llon, llcrnrlat = llat, urcrnrlon = ulon, urcrnrlat = ulat) 
        gen_basemap_plot(bm, xi, yi, zi)

    return xi, yi, zi, numcols, numrows, z_col, interp_method

if __name__ == '__main__':
    z_col='AS_ICP40'
    save='data/temp.pkl'
    xi, yi, zi = interpolate_z_col(z_col=z_col, save=save)
    xil, yil, zil, numcols, numrows, z_coll, interp_method = \
        load_interpolated_z_col(save, gen_plot=True)
    assert np.array_equal(xi,xil)
    assert np.array_equal(yi,yil)
    assert np.array_equal(zi,zil,equal_nan=True)
    assert numcols == 240
    assert numrows == 241
    assert z_coll == z_col
    assert interp_method == 'cubic'
