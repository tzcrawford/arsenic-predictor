#!/usr/bin/env python
from flask import Flask, render_template, request, redirect, url_for
import plot

app = Flask(__name__)

@app.errorhandler(404)
def not_found(e):
    return render_template("404.html")


@app.route("/arsenic/", methods=['GET', 'POST'])
def arsenic():
    if request.method == "POST":
        options = request.form
        print(options)
        chart_html = plot.main(options) #get the plot with the options we specify
        return render_template('plot.html',chart=chart_html)
    else:
        chart_html = plot.main(dict()) #pass an empty dict to make a blank plot
        return render_template('plot.html',chart=chart_html)

@app.route("/")
def index():
    return redirect(url_for("arsenic"))

if __name__ == "__main__":
    #print("Run with `flask` or a WSGI server!")
    app.run() #run the application on the local dev server
